<?
    $company_email = "developer@andsmi.ru, darbinyanvahan@gmail.com";
    $subject = "Заявка с сайта столярной мастерской 'Добряк' - кнопка 'Заказать'";
    $headers = "Content-Type: text/html; charset=utf-8\r\n";
    $headers .= "From: <darbinyanvahan@gmail.com>\r\n";
    $headers .= "Reply-To: darbinyanvahan@gmail.com\r\n";
    if(isset($_POST)){
        $user_name = htmlentities(trim($_POST["userName"]));
        $user_phone = htmlentities(trim($_POST["userPhone"]));
        $user_comment = htmlentities(trim($_POST["userText"]));
        $user_agree = $_POST["userAgree"];
        if(!empty($user_name) & $user_phone !== "+7(" & $user_agree == "on") {
           $message = '
            <h1>
                Вам пришло письмо с сайта <a href="http://dobryak.22web.org" target="_blank">http://dobryak.22web.org</a>
            </h1>
            <h2>
                Данное письмо пришло из модального окна по кнопке "Заказать"
            </h2>
            <h3>
                Клиент оставил следующие данные:
            </h3>
            <p>
                Вашего клиента зовут: '.$user_name.'<br/>
                Номер телефона клиента: '.$user_phone.'<br/>
                Клиент также оставил Вам свое сообщение: <br/>
                '.$user_comment.'
            </p>
            <p>
                Если нужно больше информации - наберите своему клиенту и решите с ним все вопросы по поводу его заказа
            </p>
            ';
            if(mail($company_email, $subject, $message, $headers)){
                header('Content-type: application/json');
                echo  '{"pattern": "<div class=\"callback-wrapper_thanks\"><h2 class=\"callback-title_thanks\">Спасибо!</h2><h3 class=\"callback-title_thanks-more\">Мы свяжемся с Вами в ближайшее время</h3><button class=\"callback-close\" id=\"modalCallbackCloseBtnThanks\" type=\"button\"></button></div><div class=\"callback-pattern\"></div> ", "isFailed":false}';
            }
            else {
               header('Content-type: application/json');
               echo  '{"pattern": "Письмо не отправлено", "isFailed":true}';
            }
        }
        else if(!empty($user_name) & $user_phone === "+7(" & $user_agree == "on") {
            header('Content-type: application/json');
            echo  '{"pattern": "Пожалуйста, заполните Телефон!", "isFailed":true}';
        }
        else if(empty($user_name) & $user_phone !== "+7(" & $user_agree == "on") {
            header('Content-type: application/json');
            echo  '{"pattern": "Пожалуйста, заполните Имя!", "isFailed":true}';
        }
        else if(!empty($user_name) & $user_phone !== "+7(" & $user_agree != "on") {
            header('Content-type: application/json');
            echo  '{"pattern": "Пожалуйста примите согласие на обработку персональных данных!", "isFailed":true}';
        }
        else if(!empty($user_name) & $user_phone === "+7(" & $user_agree != "on") {
            header('Content-type: application/json');
            echo  '{"pattern": "Пожалуйста, укажите свой телефон и примите согласие на обработку персональных данных!", "isFailed":true}';
        }
        else if(empty($user_name) & $user_phone !== "+7(" & $user_agree != "on") {
            header('Content-type: application/json');
            echo  '{"pattern": "Пожалуйста, укажите свое Имя и примите согласие на обработку персональных данных!", "isFailed":true}';
        }
        else if(empty($user_name) & $user_phone === "+7(" & $user_agree == "on") {
            header('Content-type: application/json');
            echo  '{"pattern": "Пожалуйста, укажите свое Имя и Телефон!", "isFailed":true}';
        }
        else {
            header('Content-type: application/json');
            echo  '{"pattern": "Пожалуйста, заполните Имя, Телефон и дайте согласие на обработку персональных данных!", "isFailed":true}';
        }
    }
    else {
        header('Content-type: application/json');
        echo  '{"pattern": "Форма не заполнена", "isFailed":true}';
    }
