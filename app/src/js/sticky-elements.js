;
'use strict';
window.addEventListener("load", () => {
    let lastWindow;
    let windowHeight;
    const distance = window.pageYOffset;
    const deviceHeight = window.outerHeight;
    const deviceWidth = window.outerWidth;
    const pageHeight = document.documentElement.scrollHeight;
    const stickyHeader = document.getElementById("jsStickyHeader");
    const callbackWrapper = document.getElementById("footCallback");

    if(deviceWidth < 992) {
        if (distance > 100) {
            stickyHeader.classList.add("fixed");
        }
    }

    if(distance > deviceHeight) {
        callbackWrapper.classList.add("fixed");
    }

    window.onscroll = (e) => {
        windowHeight = window.scrollY;
        lastWindow = pageHeight - windowHeight;
        if (lastWindow > (deviceHeight + 300)) {
            callbackWrapper.classList.add("fixed");
        } else {
            callbackWrapper.classList.remove("fixed");
        }
        if(lastWindow<pageHeight) {
            if (deviceWidth < 992) {
                if (windowHeight > 5) {
                    stickyHeader.classList.add("fixed");
                } else {
                    stickyHeader.classList.remove("fixed");
                }
            }
        }
    }
});


