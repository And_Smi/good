;
'use strict';
window.addEventListener("DOMContentLoaded", () => {
    const deviceWidth = window.outerWidth;
    const modal = document.getElementById("modalCallback");
    const modalBg = document.getElementById("modalBackground");
    const mainWindow = document.getElementsByTagName("body")[0];
    const closeModalBtn = document.getElementById("modalCallbackCloseBtn");
    const openModalBtn = document.getElementsByClassName("callback_btn");
    
    closeModalBtn.onclick = (e) => {
        modal.style.top = "100%";
        mainWindow.style.overflowY = "auto";
        if(deviceWidth > 991) {
            modalBg.style.opacity = "0";
            modalBg.style.zIndex = "0";
        }
    };
    
    for (let o = 0; o < openModalBtn.length; o++) {
        openModalBtn[o].onclick = (e) => {
            modal.style.top = "0";
            mainWindow.style.overflowY = "hidden";
            if(deviceWidth > 991) {
                modalBg.style.opacity = "1";
                modalBg.style.zIndex = "2";
            }
        }
    }
    
    const callbackForm = modal.getElementsByClassName("callback-form")[0];
    
    callbackForm.addEventListener('submit', function(e) {
        e.preventDefault();
        
        const formData = new FormData(this);
        
        ajaxSend(formData);
    });
    
    function status(data) {
        if (data.isFailed) {
            return Promise.reject(data.pattern);
        } else {
            return Promise.resolve(data.pattern);
        }
    }
    
    const ajaxSend = (formData) => {
        fetch('/callback.php', {
            method: 'post',
            body: formData
        }).then((response) => {
            return response.json();
        }).then(status)
            .then((response) => {
                callbackForm.reset();
                let callbackWrapper = document.getElementsByClassName("callback-wrapper")[0];
                callbackWrapper.innerHTML = response;
                document.getElementById("modalCallbackCloseBtnThanks").onclick = (e) => {
                    modal.style.top = "100%";
                    mainWindow.style.overflowY = "auto";
                    if(deviceWidth > 991) {
                        modalBg.style.opacity = "0";
                        modalBg.style.zIndex = "0";
                    }
                };
            })
            .catch((error) => {
            document.getElementById("note").innerHTML = "<div class='error'>"+error+"</div>";
        })
    };
});