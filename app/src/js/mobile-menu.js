;
'use strict';
window.addEventListener("DOMContentLoaded", () => {
  const close = document.getElementById("mMenuCloseBtn");
  const burger = document.getElementById("mobileMenuBtn");
  const returnBtn = document.getElementById("innerMenuReturnBtn");
  const mobileMenu = document.getElementsByClassName("mobile-menu")[0];
  const mMenuWithInner = mobileMenu.getElementsByClassName("has-menu");
  
  burger.onclick = (e) => {
    mobileMenu.style.left = "0";
  };
  
  for(let im=0; im<mMenuWithInner.length; im++) {
    mMenuWithInner[im].onclick = function(e) {
      const iMenu = this;
      iMenu.querySelector(".mobile-inner-menu").style.left="0";
      returnBtn.style.left="15px";
      returnBtn.onclick = (e) => {
        iMenu.querySelector(".mobile-inner-menu").style.left="100%";
        returnBtn.style.left="100%";
      };
    };
  }
  
  close.onclick = (e) => {
    mobileMenu.style.left = "100%";
  };
  
});