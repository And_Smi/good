var gulp=require("gulp"),
    pug=require("gulp-pug"),
    prefix=require("gulp-autoprefixer"),
    imgMin=require("gulp-imagemin"),
    styl=require("gulp-stylus"),
    sync=require("browser-sync"),
    rename=require("gulp-rename"),
    concat=require("gulp-concat"),
    fs = require('fs'),
    data = require('gulp-data'),
    uglJS=require("gulp-uglify-es").default,
    pngQuant=require("imagemin-pngquant"),
    pathRoot = {
        "dev": "./app",
        "prod":"./public"
    },
    pathSubroot={
        "devImg":pathRoot.dev+"/src/img/**/**",
        "devFonts":pathRoot.dev+"/src/fonts/**/**",
        "devJs":pathRoot.dev+"/src/js/**/**",
        "devStyl":pathRoot.dev+"/src/styl/all.styl",
        "prodImg":pathRoot.prod+"/img/",
        "prodFonts":pathRoot.prod+/fonts/,
        "prodJs":pathRoot.prod+"/js/",
        "prodCss":pathRoot.prod+"/css/",
    };

gulp.task("stylus", function(){
    return gulp.src(pathSubroot.devStyl)
        .pipe(styl({
            compress: true,
            'include css': true
        }))
        .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7']))
        .pipe(rename("styles.min.css"))
        .pipe(gulp.dest(pathSubroot.prodCss));
});

gulp.task("img", function(){
    return gulp.src(pathSubroot.devImg)
        .pipe(imgMin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngQuant()]
        }))
        .pipe(gulp.dest(pathSubroot.prodImg))
});

gulp.task("fonts", function(){
    return gulp.src(pathSubroot.devFonts)
        .pipe(gulp.dest(pathSubroot.prodFonts));
});

gulp.task("es", function(){
    return gulp.src(pathSubroot.devJs)
        .pipe(uglJS())
        .pipe(concat("app.min.js"))
        .pipe(gulp.dest(pathSubroot.prodJs))
});

gulp.task("pug", function(){
    return gulp.src(["app/**/*.pug", "!app/tmpl/**/*.pug", "!app/modules/**/*.pug"])
        .pipe(data(function(file) {
            return JSON.parse(fs.readFileSync('data/data.json', 'utf8'))
        }))
        .pipe(pug({
            pretty: true,
            locals: 'data/data.json'
        }))
        .pipe(gulp.dest(pathRoot.prod));
});

gulp.task("php", function(){
    return gulp.src("./app/backend/modules/callback.php")
        .pipe(gulp.dest(pathRoot.prod));
});

gulp.task("build", gulp.series(
    [
        gulp.parallel("pug", "es", "stylus", "php"),
        gulp.parallel("img")
    ]
));

gulp.task("watch", function(){
    gulp.watch(pathSubroot.prodJs, sync.reload);
    gulp.watch(pathSubroot.prodCss, sync.reload);
    gulp.watch(pathSubroot.devJs, gulp.series("es"));
    gulp.watch(pathRoot.prod+"**/*.html", sync.reload);
    gulp.watch("./data/data.json", gulp.series("pug"));
    gulp.watch(pathRoot.dev+"/**/*.pug", gulp.series("pug"));
    gulp.watch(pathRoot.dev+"/src/styl/**/**.styl", gulp.series("stylus"));
    gulp.watch(pathRoot.dev+"/backend/modules/callback.php", gulp.series("php"));
});

gulp.task("serve", function(){
    sync({
        server: {
            baseDir: "./public"
        },
        notify: false
    });
    sync.watch("./app", sync.reload);
});

gulp.task("default", gulp.series(
    [
        gulp.parallel('stylus', 'fonts', 'es', 'pug', 'php'),
        gulp.parallel('serve', 'watch')
    ]
));